# About
Api that handles information about products and their specifications.
It connects to two external services to bring additional data:
1. `Brand Service`
2. `Category Service`: This one is cached for 1 minute before evicting the cached data

# Docs and Auditing
Swagger is supported on context route `/swagger-ui/`. At the same time, to monitor the health of the system the url
`/actuator/health` can be used. For tracing, the API creates a log file on the root of the execution path and the last 1000
requests can be viewed with the endpoint `/actuator/httptrace`.

# Build
The project can be build using the maven wrapper in the source. Java 11 is needed

cmd: `.\mvnw clean install`
linux: `./mvnw clean install`

At the same time, by default, the API will load some mock data if the profile is left for default or the `test` profile 
is used.