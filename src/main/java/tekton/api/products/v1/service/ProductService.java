package tekton.api.products.v1.service;

import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import tekton.api.products.domain.model.dto.Brand;
import tekton.api.products.domain.model.dto.Category;
import tekton.api.products.domain.model.dto.Product;
import tekton.api.products.domain.model.dto.ProductBase;
import tekton.api.products.domain.model.dto.Specification;
import tekton.api.products.domain.model.entity.ProductEntity;
import tekton.api.products.domain.model.entity.SpecificationEntity;
import tekton.api.products.domain.repository.IBrandRepository;
import tekton.api.products.domain.repository.ICategoryRepository;
import tekton.api.products.domain.repository.IProductRepository;
import tekton.api.products.domain.service.IProductService;

@Service
public class ProductService implements IProductService {
    @Autowired IProductRepository productRepository;
    @Autowired IBrandRepository brandRepository;
    @Autowired ICategoryRepository categoryRepository;

    @Override
    public Product findById(long id) {
        var entity = productRepository.findById(id)
            .orElseThrow(() -> new ResponseStatusException(
                HttpStatus.NOT_FOUND,
                "There's no product with id " + id
            ));

        var brand = brandRepository.findById(entity.getBrandId());
        var category = categoryRepository.findById(entity.getCategoryId());

        return fromEntity(entity, brand, category);
    }
    
    @Override
    public Product create(ProductBase data) {
        var brand = brandRepository.findById(data.getBrandId())
            .orElseThrow(() -> new ResponseStatusException (
                HttpStatus.BAD_REQUEST,
                "There's no brand with identifier " + data.getBrandId()
            ));
        
        var category = categoryRepository.findById(data.getCategoryId())
            .orElseThrow(() -> new ResponseStatusException(
                HttpStatus.BAD_REQUEST,
                "There's no category with identifier " + data.getCategoryId()
            ));
        
        if (data.getSpecifications() == null || data.getSpecifications().isEmpty()) 
            throw new ResponseStatusException(
                HttpStatus.BAD_REQUEST,
                "You must send at least one specification"
            );

        var entity = new ProductEntity();
        entity.setId((long) 0);
        entity.setName(data.getName());
        entity.setBasePrice(data.getBasePrice());
        entity.setBrandId(data.getBrandId());
        entity.setCategoryId(data.getBrandId());

        entity = productRepository.save(entity);

        final var ref = entity;
        entity.setSpecifications(data.getSpecifications().stream()
            .map(spec -> {
                var specEntity = new SpecificationEntity();
                specEntity.setId((long) 0);
                specEntity.setSize(spec.getSize());
                specEntity.setSpecialPrice(spec.getSpecialPrice());
                specEntity.setStock(spec.getStock());
                specEntity.setColor(spec.getColor());
                specEntity.setRef(ref);

                return specEntity;
            })
            .collect(Collectors.toSet())
        );
        
        entity = productRepository.save(entity);
        
        return fromEntity(entity, Optional.of(brand), Optional.of(category));
    }

    @Override
    public void update(long id, ProductBase updateData) {
        var entity = productRepository.findById(id)
            .orElseThrow(() -> new ResponseStatusException(
                HttpStatus.NOT_FOUND,
                "There's no product with id " + id
            ));
            
        entity.setBasePrice(updateData.getBasePrice());
        entity.setName(updateData.getName());

        if (entity.getBrandId() != updateData.getBrandId()) {
            brandRepository.findById(updateData.getBrandId())
                .orElseThrow(() -> new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,
                    "There's no brand with identifier " + updateData.getBrandId()
                ));
            entity.setBrandId(updateData.getBrandId());
        }
        if (entity.getCategoryId() != updateData.getCategoryId()) {
            categoryRepository.findById(updateData.getCategoryId())
                .orElseThrow(() -> new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,
                    "There's no category with identifier " + updateData.getCategoryId()
                ));
            entity.setCategoryId(updateData.getCategoryId());
        }
        updateData.getSpecifications().forEach(spec -> {
            var optional = entity.getSpecifications()
                .stream()
                .filter(i -> i.getId() == spec.getId())
                .findFirst();
            if (optional.isPresent()) {
                var specEntity = optional.get();
                specEntity.setSize(spec.getSize());
                specEntity.setSpecialPrice(spec.getSpecialPrice());
                specEntity.setStock(spec.getStock());
                specEntity.setColor(spec.getColor());
            }
            else {
                var specEntity = new SpecificationEntity();
                specEntity.setId((long) 0);
                specEntity.setSize(spec.getSize());
                specEntity.setSpecialPrice(spec.getSpecialPrice());
                specEntity.setStock(spec.getStock());
                specEntity.setColor(spec.getColor());
                specEntity.setRef(entity);

                entity.getSpecifications().add(specEntity);
            }
        });

        productRepository.save(entity);
    }

    
    Product fromEntity(ProductEntity entity, Optional<Brand> brand, Optional<Category> category) {        
        var product = new Product();

        product.setId(entity.getId());
        product.setBasePrice(entity.getBasePrice());
        product.setName(entity.getName());
        product.setBrand(brand.orElseGet(() -> {
            var defaultBrand = new Brand();
            defaultBrand.setId(entity.getBrandId());
            defaultBrand.setName("Information about the brand couldn't be found");

            return defaultBrand;
        }));
        product.setCategory(category.orElseGet(() -> {
            var defaultCategory = new Category();
            defaultCategory.setId(entity.getCategoryId());
            defaultCategory.setName("Information about the category couldn't be found");

            return defaultCategory;
        }));
        product.setSpecifications(entity.getSpecifications()
            .stream()
            .map(specEntity -> {
                var spec = new Specification();
                spec.setId(specEntity.getId());
                spec.setSize(specEntity.getSize());
                spec.setSpecialPrice(specEntity.getSpecialPrice());
                spec.setStock(specEntity.getStock());
                spec.setColor(specEntity.getColor());

                return spec;
            })
            .collect(Collectors.toList()));

        return product;
    }
}
