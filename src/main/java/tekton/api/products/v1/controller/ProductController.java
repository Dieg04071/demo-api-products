package tekton.api.products.v1.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tekton.api.products.domain.model.dto.Product;
import tekton.api.products.domain.model.dto.ProductBase;
import tekton.api.products.domain.service.IProductService;

import javax.validation.Valid;

@RestController
@RequestMapping(path = "/v1/products")
public class ProductController {
    @Autowired
    IProductService service;

    @GetMapping(path = "/{id}")
    public Product findById( @PathVariable long id ) {
        return service.findById(id);
    }

    @PutMapping(path = "/{id}")
    public ResponseEntity<?> updateById(@PathVariable long id, @RequestBody @Valid ProductBase updateData ) {
        service.update(id, updateData);

        return ResponseEntity.noContent().build();
    }

    @PostMapping(path = "")
    public Product create( @RequestBody @Valid ProductBase data ) {
        return service.create(data);
    }

}
