package tekton.api.products.http.repository;

import java.util.Optional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

import tekton.api.products.domain.model.dto.Category;
import tekton.api.products.domain.repository.ICategoryRepository;

@Repository
public class HttpCategoryRepository implements ICategoryRepository {
    @Autowired @Qualifier("category-client") RestTemplate template;
    @Autowired RetryTemplate retryTemplate;
    @Autowired ObjectMapper mapper;

    @Override

    @Cacheable(value = "category", key = "#id")
    public Optional<Category> findById(long id) {
        try {
            return Optional.of(cacheableFindById(id));
        }
        catch(Exception e) {
            return Optional.empty();
        }
    }

    public Category cacheableFindById(long id) {
        var response = retryTemplate.execute(
            context -> template.exchange("/" + id, HttpMethod.GET, null, String.class)
        );

        if (response.getStatusCode() != HttpStatus.OK) {
            throw new ResponseStatusException(
                response.getStatusCode(),
                "Failed to obtain category info with id " + id
            );
        }

        try {
            return mapper.readValue(response.getBody(), Category.class);
        } catch ( JsonProcessingException e) {
            throw new ResponseStatusException(
                HttpStatus.INTERNAL_SERVER_ERROR,
                "Failed to parse response for category " + id,
                e
            );
        }
    }
    
    @Scheduled(fixedRate = 60000)
    @CacheEvict(value = "category", allEntries = true)
    public void clearCache() {   
           
    }
    
}
