package tekton.api.products.http.repository;

import java.util.Optional;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

import tekton.api.products.domain.model.dto.Brand;
import tekton.api.products.domain.repository.IBrandRepository;

@Repository
public class HttpBrandRepository implements IBrandRepository {
    @Autowired @Qualifier("brand-client") RestTemplate template;
    @Autowired RetryTemplate retryTemplate;
    @Autowired ObjectMapper mapper;
    
    @Override
    public Optional<Brand> findById(long id) {
        try {
            var response = retryTemplate.execute(
                context -> template.exchange("/" + id, HttpMethod.GET, null, String.class)
            );

            if (response.getStatusCode() != HttpStatus.OK) {
                throw new ResponseStatusException(
                    response.getStatusCode(),
                    "Failed to obtain brand info with id " + id
                );
            }

            return Optional.of(mapper.readValue(response.getBody(), Brand.class));
        }
        catch(Exception e) {
            return Optional.empty();
        }
    }
}
