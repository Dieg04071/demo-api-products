package tekton.api.products.domain.model.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Entity(name =  "products")
@EqualsAndHashCode(exclude = "specifications")
public class ProductEntity {
    @Id
    @Column(name = "product_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;
    
    @Column
    private double basePrice;
    
    @Column
    private Long brandId;
    
    @Column
    private Long categoryId;
    
    @OneToMany(
        mappedBy = "ref",
        cascade = CascadeType.ALL
    )
    private Set< SpecificationEntity > specifications;
}
