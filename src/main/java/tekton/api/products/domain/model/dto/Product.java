package tekton.api.products.domain.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Product extends ProductBase {

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Brand brand;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Category category;

}
