package tekton.api.products.domain.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Specification {
    private Long id;

    @NotNull(message = "The stock must be set")
    private Integer stock;
    
    private Double specialPrice;
    
    private String color;
    
    private String size;
}
