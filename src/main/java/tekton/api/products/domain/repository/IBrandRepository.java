package tekton.api.products.domain.repository;

import java.util.Optional;

import tekton.api.products.domain.model.dto.Brand;

public interface IBrandRepository {
    Optional<Brand> findById(long id);
}
