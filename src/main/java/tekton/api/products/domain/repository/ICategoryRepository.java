package tekton.api.products.domain.repository;

import java.util.Optional;

import tekton.api.products.domain.model.dto.Category;

public interface ICategoryRepository {
    Optional<Category> findById(long id);
}
