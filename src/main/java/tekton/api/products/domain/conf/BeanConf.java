package tekton.api.products.domain.conf;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.classify.Classifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.retry.RetryPolicy;
import org.springframework.retry.policy.ExceptionClassifierRetryPolicy;
import org.springframework.retry.policy.NeverRetryPolicy;
import org.springframework.retry.policy.SimpleRetryPolicy;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriBuilderFactory;

import tekton.api.products.domain.conf.properties.BrandApiProperties;
import tekton.api.products.domain.conf.properties.CategoryApiProperties;

import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.http.HttpStatus;

@Configuration
public class BeanConf {
    private final SimpleRetryPolicy simpleRetryPolicy = new SimpleRetryPolicy(3);
    private final NeverRetryPolicy neverRetryPolicy = new NeverRetryPolicy();
    
    @Autowired BrandApiProperties brandApiProperties;
    @Autowired CategoryApiProperties categoryApiProperties;

    @Bean
    @Primary
    public ObjectMapper mapper() {
        return new ObjectMapper()
            .registerModule(new Jdk8Module())
            .registerModule(new JavaTimeModule())
            .registerModule(new ParameterNamesModule());
    }

    @Bean("brand-client")
    public RestTemplate restTemplateBrand() {
        var template = new RestTemplate();
        template.setUriTemplateHandler(new DefaultUriBuilderFactory(brandApiProperties.getBaseUrl()));
        
        return template;
    }

    @Bean("category-client")
    public RestTemplate restTemplateCategory() {
        var template = new RestTemplate();
        template.setUriTemplateHandler(new DefaultUriBuilderFactory(categoryApiProperties.getBaseUrl()));
        
        return template;
    }

    @Bean
    public RetryTemplate retryTemplate() {
        var retryTemplate = new RetryTemplate();
        var policy = new ExceptionClassifierRetryPolicy();
        policy.setExceptionClassifier(configureStatusCodeBasedRetryPolicy());
        retryTemplate.setRetryPolicy(policy);
        return retryTemplate;
    }
    
    private Classifier<Throwable, RetryPolicy> configureStatusCodeBasedRetryPolicy() {
        return throwable -> {
            if (throwable instanceof HttpStatusCodeException) {
                var exception = (HttpStatusCodeException) throwable;
                return getRetryPolicyForStatus(exception.getStatusCode());
            }
            return simpleRetryPolicy;
        };
    }

    private RetryPolicy getRetryPolicyForStatus(HttpStatus httpStatus) {
        switch (httpStatus) {
            case BAD_GATEWAY:
            case SERVICE_UNAVAILABLE:
            case INTERNAL_SERVER_ERROR:
            case GATEWAY_TIMEOUT:
                return simpleRetryPolicy;
            default:
                return neverRetryPolicy;
        }
    }
}
