package tekton.api.products.domain.conf.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Data;

@Data
@Configuration
@ConfigurationProperties(prefix = "api.brand") 
public class BrandApiProperties {
    private String baseUrl;
}
