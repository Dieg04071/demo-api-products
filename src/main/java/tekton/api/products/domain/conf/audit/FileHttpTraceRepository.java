package tekton.api.products.domain.conf.audit;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.stream.Collectors;

import org.springframework.boot.actuate.trace.http.HttpTrace;
import org.springframework.boot.actuate.trace.http.HttpTraceRepository;
import org.springframework.stereotype.Component;

import lombok.extern.log4j.Log4j2;

@Log4j2
@Component
public class FileHttpTraceRepository implements HttpTraceRepository {
    private ConcurrentLinkedQueue<HttpTrace> data = new ConcurrentLinkedQueue<>(); 

    @Override
    public void add(HttpTrace trace) {
        if (data.size() > 1000) {
            data.poll();
        }
        data.add(trace);
        
        if ( trace.getRequest().getUri().toString().contains("/v1") ) {
            log.info("Time for GET: " + trace.getTimeTaken());
            try {
                var content = String.format("Uri [%s] Method [%s] Time (ms) [%d] Res Status [%d]",
                    trace.getRequest().getUri().toString(),
                    trace.getRequest().getMethod(),
                    trace.getTimeTaken(),
                    trace.getResponse().getStatus()
                ) + System.lineSeparator();

                Files.write(
                    Paths.get("logs.txt"), 
                    content.getBytes(), 
                    StandardOpenOption.CREATE,
                    StandardOpenOption.APPEND
                );
            }
            catch(Exception e) {}
        }
    }

    @Override
    public List<HttpTrace> findAll() {
        return data.stream()
            .collect(Collectors.toList());
    } 
    
}
