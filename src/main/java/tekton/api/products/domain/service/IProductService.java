package tekton.api.products.domain.service;

import tekton.api.products.domain.model.dto.Product;
import tekton.api.products.domain.model.dto.ProductBase;

public interface IProductService {
    Product findById(long id);

    void update(long id, ProductBase updateData);

    Product create(ProductBase data);
}
