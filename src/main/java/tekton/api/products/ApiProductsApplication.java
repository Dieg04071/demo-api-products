package tekton.api.products;

import java.util.HashSet;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;

import tekton.api.products.domain.model.entity.ProductEntity;
import tekton.api.products.domain.model.entity.SpecificationEntity;
import tekton.api.products.domain.repository.IProductRepository;

@SpringBootApplication
public class ApiProductsApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiProductsApplication.class, args);
	}

	@Bean 
	@Profile(value = { "dev", "test", "integration" })
	public CommandLineRunner demoData(IProductRepository repository) {
		return (args) -> {
			var product01 = new ProductEntity();
			product01.setName("Sports Shirt");
			product01.setBasePrice(120.0);
			product01.setBrandId((long) 1);
			product01.setCategoryId((long) 1);
			product01.setSpecifications(new HashSet<>());

			var product02 = new ProductEntity();
			product02.setName("Joggers");
			product02.setBasePrice(120.0);
			product02.setBrandId((long) 1);
			product02.setCategoryId((long) 2);
			product02.setSpecifications(new HashSet<>());
			
			var product03 = new ProductEntity();
			product03.setName("Socks");
			product03.setBasePrice(120.0);
			product03.setBrandId((long) 2);
			product03.setCategoryId((long) 1);
			product03.setSpecifications(new HashSet<>());

			product01 = repository.save(product01);
			product02 = repository.save(product02);
			product03 = repository.save(product03);

			var spec01 = new SpecificationEntity();
			spec01.setColor("blue");
			spec01.setSize("S");
			spec01.setStock(35);
			spec01.setSpecialPrice(140.50);
			spec01.setRef(product01);
			product01.getSpecifications().add(spec01);

			var spec02 = new SpecificationEntity();
			spec02.setColor("red");
			spec02.setSize("XL");
			spec02.setStock(40);
			spec02.setRef(product01);
			product01.getSpecifications().add(spec02);

			var spec03 = new SpecificationEntity();
			spec03.setColor("gray");
			spec03.setSize("M");
			spec03.setStock(30);
			spec03.setRef(product02);
			product02.getSpecifications().add(spec03);

			var spec04 = new SpecificationEntity();
			spec04.setColor("black");
			spec04.setSize("S");
			spec04.setStock(50);
			spec04.setRef(product03);
			product03.getSpecifications().add(spec04);

			product01 = repository.save(product01);
			product02 = repository.save(product02);
			product03 = repository.save(product03);
		};
	}
}
