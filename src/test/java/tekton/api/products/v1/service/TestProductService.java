package tekton.api.products.v1.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.server.ResponseStatusException;

import tekton.api.products.domain.model.dto.Brand;
import tekton.api.products.domain.model.dto.Category;
import tekton.api.products.domain.model.dto.ProductBase;
import tekton.api.products.domain.model.dto.Specification;
import tekton.api.products.domain.model.entity.ProductEntity;
import tekton.api.products.domain.model.entity.SpecificationEntity;
import tekton.api.products.domain.repository.IBrandRepository;
import tekton.api.products.domain.repository.ICategoryRepository;
import tekton.api.products.domain.repository.IProductRepository;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;

@ExtendWith(SpringExtension.class)
public class TestProductService {

    @TestConfiguration
    static class TestProductServiceConf {
        @Bean public ProductService productService() {
            return new ProductService();
        }
    }

    @Autowired ProductService service;

    @MockBean IBrandRepository brandRepository;
    @MockBean ICategoryRepository categoryRepository;
    @MockBean IProductRepository productRepository;

    @BeforeEach
    public void mock() {
        when(productRepository.findById((long)1)).thenReturn(Optional.of(simpleEntity()));
        when(productRepository.findById((long)2)).thenReturn(Optional.of(simpleEntityWithoutBrand()));
        when(productRepository.findById((long)3)).thenReturn(Optional.of(simpleEntityWithoutCategory()));
        when(productRepository.findById((long)4)).thenReturn(Optional.empty());
        when(productRepository.save(any())).thenAnswer((ctx) -> ctx.getArgument(0));

        when(categoryRepository.findById((long)1)).thenReturn(Optional.of(simpleCategory()));
        when(categoryRepository.findById((long)2)).thenReturn(Optional.empty());

        when(brandRepository.findById((long)1)).thenReturn(Optional.of(simpleBrand()));
        when(brandRepository.findById((long)2)).thenReturn(Optional.empty());
    }
    
    @Test
    public void testFindByIdWithBrandAndCategoryData() {
        var product = service.findById(1);

        assertNotEquals(product.getBrand().getName(), "Information about the brand couldn't be found" );
        assertNotEquals(product.getCategory().getName(), "Information about the category couldn't be found" );
    }
    
    @Test
    public void testFindByIdWithouBrandInfo() {
        var product = service.findById(2);

        assertEquals(product.getBrand().getName(), "Information about the brand couldn't be found" );
        assertNotEquals(product.getCategory().getName(), "Information about the category couldn't be found" );
    }
    
    @Test
    public void testFindByIdWithouCategoryInfo() {
        var product = service.findById(3);

        assertNotEquals(product.getBrand().getName(), "Information about the brand couldn't be found" );
        assertEquals(product.getCategory().getName(), "Information about the category couldn't be found" );
    }

    @Test
    public void testNotFound() {
        assertThrows(ResponseStatusException.class, () -> service.findById(4));
    }
    
    @Test
    public void testCreate() {
        assertThrows(ResponseStatusException.class, () -> service.create(noSpecifications()));
        assertThrows(ResponseStatusException.class, () -> service.create(nonExistingBrand()));
        assertThrows(ResponseStatusException.class, () -> service.create(nonExistingCategory()));
        
        var validProduct = validProduct();
        var response = service.create(validProduct());

        assertEquals(response.getName(), validProduct.getName());
        assertEquals(response.getBasePrice(), validProduct.getBasePrice());
        assertEquals(response.getSpecifications().size(), validProduct.getSpecifications().size());
        assertEquals(response.getBrand().getId(), validProduct.getBrandId());
        assertEquals(response.getCategory().getId(), validProduct.getCategoryId());
    }

    @Test
    public void testUpdateNotFound() {
        assertThrows(ResponseStatusException.class, () -> service.update(4, new ProductBase()));
    }

    @Test
    public void testUpdateWithoutSpecificationBrandData() {
        var updateData = validProduct();
        updateData.setName("UPDATED NAME");
        updateData.setBasePrice(100.0);

        service.update(1, updateData);

        var entity = service.findById(1);
        assertEquals(entity.getName(), "UPDATED NAME");
        assertEquals(entity.getBasePrice(), 100.0);
    }

    @Test
    public void testUpdateWithNonExistingBrand() {
        var updateData = validProduct();
        updateData.setName("UPDATED NAME");
        updateData.setBasePrice(100.0);
        updateData.setBrandId((long) 2);

        assertThrows(ResponseStatusException.class, () -> service.update(1, updateData));
    }

    @Test
    public void testUpdateWithNonExistingCategory() {
        var updateData = validProduct();
        updateData.setName("UPDATED NAME");
        updateData.setBasePrice(100.0);
        updateData.setCategoryId((long) 2);

        assertThrows(ResponseStatusException.class, () -> service.update(1, updateData));
    }

    @Test
    public void testUpdateExistingBrand() {
        var updateData = validProduct();
        updateData.setName("UPDATED NAME");
        updateData.setBasePrice(100.0);
        updateData.setBrandId((long) 1);

        service.update(2, updateData);

        var entity = service.findById(2);
        assertEquals(entity.getName(), "UPDATED NAME");
        assertEquals(entity.getBasePrice(), 100.0);
        assertEquals(entity.getBrand().getId(), updateData.getBrandId());

    }

    @Test
    public void testUpdateExistingCategory() {
        var updateData = validProduct();
        updateData.setName("UPDATED NAME");
        updateData.setBasePrice(100.0);
        updateData.setCategoryId((long) 1);

        service.update(3, updateData);

        var entity = service.findById(3);
        assertEquals(entity.getName(), "UPDATED NAME");
        assertEquals(entity.getBasePrice(), 100.0);
        assertEquals(entity.getCategory().getId(), updateData.getCategoryId());

    }

    @Test
    public void testUpdateAddSpecification() {
        var updateData = validProduct();
        updateData.setName("UPDATED NAME");
        updateData.setBasePrice(100.0);

        var specification = new Specification();
        specification.setId((long)0);
        specification.setSize("new size");
        specification.setStock(150);
        specification.setSpecialPrice(10.0);
        specification.setColor("new color");

        updateData.setSpecifications(
            Arrays.asList(specification)
        );

        service.update(1, updateData);
        var data = service.findById(1);
        assertEquals(data.getSpecifications().size(), 2);
        assertEquals(data.getSpecifications().get(1), specification);
    }

    @Test
    public void testUpdateModifySpecification() {
        var updateData = validProduct();
        updateData.setName("UPDATED NAME");
        updateData.setBasePrice(100.0);

        var specification = new Specification();
        specification.setId((long)1);
        specification.setSize("new size");
        specification.setStock(150);
        specification.setSpecialPrice(10.0);
        specification.setColor("new color");

        updateData.setSpecifications(
            Arrays.asList(specification)
        );

        service.update(1, updateData);
        
        var data = service.findById(1);
        assertEquals(data.getSpecifications().size(), 1);
        assertEquals(data.getSpecifications().get(0), specification);
    }

    ProductEntity simpleEntity() {
        var entity = new ProductEntity();

        entity.setId((long)1);
        entity.setName("Product 01");
        entity.setBasePrice(10.0);
        entity.setCategoryId((long) 1);
        entity.setBrandId((long) 1);
        entity.setSpecifications(entitySpecifications(1));

        return entity;
    }

    ProductEntity simpleEntityWithoutBrand() {
        var entity = new ProductEntity();

        entity.setId((long)2);
        entity.setName("Product 02");
        entity.setBasePrice(10.0);
        entity.setCategoryId((long) 1);
        entity.setBrandId((long) 2);
        entity.setSpecifications(entitySpecifications(2));

        return entity;
    }

    ProductEntity simpleEntityWithoutCategory() {
        var entity = new ProductEntity();

        entity.setId((long)3);
        entity.setName("Product 03");
        entity.setBasePrice(10.0);
        entity.setCategoryId((long) 2);
        entity.setBrandId((long) 1);
        entity.setSpecifications(entitySpecifications(3));

        return entity;
    }

    Set<SpecificationEntity> entitySpecifications(long id) {
        var entity = new SpecificationEntity();
        entity.setId(id);
        entity.setColor("color");
        entity.setSize("size");
        entity.setSpecialPrice(10.0);
        entity.setStock(10);

        var list = new LinkedHashSet<SpecificationEntity>();
        list.add(entity);

        return list;
    }

    Category simpleCategory() {
        var category = new Category();
        category.setId((long)1);
        category.setName("category nro °1");
        category.setDescription("TEST");

        return category;
    }

    Brand simpleBrand() {
        var brand = new Brand();
        brand.setId((long)1);
        brand.setLogo("link");
        brand.setName("Brand name");

        return brand;
    }

    ProductBase validProduct() {
        var base = new ProductBase();

        base.setName("Product 01");
        base.setBasePrice(10.0);
        base.setCategoryId((long) 1);
        base.setBrandId((long) 1);
        base.setSpecifications(Arrays.asList(specification(0)));

        return base;
    }

    ProductBase nonExistingCategory() {
        var base = new ProductBase();

        base.setName("Product 01");
        base.setBasePrice(10.0);
        base.setCategoryId((long) 2);
        base.setBrandId((long) 1);
        base.setSpecifications(Arrays.asList(specification(0)));

        return base;
    }

    ProductBase nonExistingBrand() {
        var base = new ProductBase();

        base.setName("Product 01");
        base.setBasePrice(10.0);
        base.setCategoryId((long) 1);
        base.setBrandId((long) 2);
        base.setSpecifications(Arrays.asList(specification(0)));

        return base;
    }

    ProductBase noSpecifications() {
        var base = new ProductBase();

        base.setName("Product 01");
        base.setBasePrice(10.0);
        base.setCategoryId((long) 1);
        base.setBrandId((long) 2);
        base.setSpecifications(Collections.emptyList());

        return base;
    }
    
    Specification specification(long id) {
        var spec = new Specification();

        spec.setId(id);
        spec.setColor("color");
        spec.setSize("size");
        spec.setSpecialPrice(10.0);
        spec.setStock(10);

        return spec;
    } 
}
