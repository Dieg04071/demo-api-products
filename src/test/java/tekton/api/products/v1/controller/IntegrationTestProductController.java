package tekton.api.products.v1.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;


@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class IntegrationTestProductController {
    @LocalServerPort
    private int port;

    TestRestTemplate restTemplate = new TestRestTemplate();
    HttpHeaders headers = new HttpHeaders();

    @Test
    public void testGetById() {
        var entity = new HttpEntity<String>(null, headers);

        var response = restTemplate.exchange(
          createURLWithPort("/v1/products/1"), HttpMethod.GET, entity, String.class);

        assertEquals(response.getStatusCode(), HttpStatus.OK);

        response = restTemplate.exchange(
          createURLWithPort("/v1/products/9999"), HttpMethod.GET, entity, String.class);

        assertEquals(response.getStatusCode(), HttpStatus.NOT_FOUND);
    }

    @Test
    public void testCreate() throws Exception {
        var request = "{\"name\":\"NEW\",\"basePrice\":20.0,\"brandId\": \"1\", \"categoryId\":\"1\",\"specifications\":[{\"stock\":1000,\"color\":\"X\",\"size\":\"X\"}]}";
        headers.setContentType(MediaType.APPLICATION_JSON);
        var entity = new HttpEntity<String>(request, headers);

        var response = restTemplate.exchange(
          createURLWithPort("/v1/products"), HttpMethod.POST, entity, String.class);

        assertEquals(response.getStatusCode(), HttpStatus.OK);
    }

    @Test
    public void testUpdate() throws Exception {
        var request = "{\"name\":\"UPDATE\",\"basePrice\":20.0,\"brandId\": \"1\", \"categoryId\":\"1\",\"specifications\":[{\"stock\":1000,\"color\":\"X\",\"size\":\"X\"}]}";
        headers.setContentType(MediaType.APPLICATION_JSON);
        var entity = new HttpEntity<String>(request, headers);

        var response = restTemplate.exchange(
          createURLWithPort("/v1/products/1"), HttpMethod.PUT, entity, String.class);

        assertEquals(response.getStatusCode(), HttpStatus.NO_CONTENT);
    }
    
    private String createURLWithPort(String uri) {
        return "http://localhost:" + port + uri;
    }

}