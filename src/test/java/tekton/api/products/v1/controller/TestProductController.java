package tekton.api.products.v1.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.server.ResponseStatusException;
import tekton.api.products.domain.model.dto.Product;
import tekton.api.products.domain.model.dto.ProductBase;
import tekton.api.products.domain.model.dto.Specification;
import tekton.api.products.domain.service.IProductService;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
public class TestProductController {
    List<Specification> specifications = Collections.singletonList(new Specification());
    List<Specification> emptySpecification = Collections.emptyList();

    @TestConfiguration
    static class TestProductControllerConf {
        @Bean public ProductController productController() {
            return new ProductController();
        }
    }

    @MockBean IProductService mockService;
    @Autowired ProductController controller;

    @BeforeEach
    public void mock() {

        when(mockService.findById(1)).thenReturn(new Product());
        when(mockService.findById(2)).thenReturn(new Product());
        when(mockService.findById(3)).thenThrow(ResponseStatusException.class);
        when(mockService.findById(4)).thenThrow(ResponseStatusException.class);

        doThrow(ResponseStatusException.class)
                .when(mockService)
                .update(eq((long) 3), any());

        doNothing()
                .when(mockService)
                .update((long) 1, validProduct());

        doThrow(ResponseStatusException.class)
                .when(mockService)
                .update((long) 1, noSpecifications());

        doThrow(ResponseStatusException.class)
                .when(mockService)
                .update((long) 1, nonExistingCategory());

        doThrow(ResponseStatusException.class)
                .when(mockService)
                .update((long) 1, nonExistingBrand());


        when(mockService.create( validProduct() )).thenReturn(new Product());
        when(mockService.create( noSpecifications() )).thenThrow(ResponseStatusException.class);
        when(mockService.create( nonExistingCategory() )).thenThrow(ResponseStatusException.class);
        when(mockService.create( nonExistingBrand() )).thenThrow(ResponseStatusException.class);

    }

    @Test
    public void testFindById() {
        assertNotNull(controller.findById(1));
        assertNotNull(controller.findById(2));
        assertThrows(ResponseStatusException.class, () -> controller.findById(3));
        assertThrows(ResponseStatusException.class, () -> controller.findById(4));
    }

    @Test
    public void testUpdate() {
        assertDoesNotThrow( () -> controller.updateById(1, validProduct()));

        assertThrows( ResponseStatusException.class, () -> controller.updateById(3, validProduct()));

        assertThrows( ResponseStatusException.class, () -> controller.updateById(1, nonExistingCategory()));

        assertThrows( ResponseStatusException.class, () -> controller.updateById(1, nonExistingBrand()));

        assertThrows( ResponseStatusException.class, () -> controller.updateById(1, noSpecifications()));
    }

    @Test
    public void testCreate() {
        assertDoesNotThrow( () -> controller.create( validProduct() ));

        assertThrows( ResponseStatusException.class, () -> controller.create( nonExistingBrand() ));

        assertThrows( ResponseStatusException.class, () -> controller.create( nonExistingCategory() ));

        assertThrows( ResponseStatusException.class, () -> controller.create( noSpecifications() ));
    }

    ProductBase validProduct() {
        var base = new ProductBase();

        base.setName("Product 01");
        base.setBasePrice(10.0);
        base.setCategoryId((long) 1);
        base.setBrandId((long) 1);
        base.setSpecifications(specifications);

        return base;
    }

    ProductBase nonExistingCategory() {
        var base = new ProductBase();

        base.setName("Product 01");
        base.setBasePrice(10.0);
        base.setCategoryId((long) 2);
        base.setBrandId((long) 1);
        base.setSpecifications(specifications);

        return base;
    }

    ProductBase nonExistingBrand() {
        var base = new ProductBase();

        base.setName("Product 01");
        base.setBasePrice(10.0);
        base.setCategoryId((long) 1);
        base.setBrandId((long) 2);
        base.setSpecifications(specifications);

        return base;
    }

    ProductBase noSpecifications() {
        var base = new ProductBase();

        base.setName("Product 01");
        base.setBasePrice(10.0);
        base.setCategoryId((long) 1);
        base.setBrandId((long) 2);
        base.setSpecifications(emptySpecification);

        return base;
    }
}
