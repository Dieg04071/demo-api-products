package tekton.api.products.http.repository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

import tekton.api.products.domain.model.dto.Category;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

import com.fasterxml.jackson.databind.ObjectMapper;

@ExtendWith(SpringExtension.class)
public class TestHttpCategoryRepository {
    @TestConfiguration
    static class TestHttpCategoryRepositoryConf {
        @Bean
        public HttpCategoryRepository httpCategoryRepository() {
            return new HttpCategoryRepository();
        }

        @Bean 
        public RetryTemplate retryTemplate() {
            return new RetryTemplate();
        }
    }

    @Autowired HttpCategoryRepository httpCategoryRepository;
    @MockBean @Qualifier("category-client") RestTemplate restTemplate;
    @MockBean ObjectMapper mapper;
    
    @BeforeEach
    public void mock() throws Throwable {
        when(mapper.readValue(any(String.class), eq(Category.class))).thenReturn(new Category());
        
        when(
            restTemplate.exchange(
                "/1",
                HttpMethod.GET, 
                null, 
                String.class
            )
        ).thenReturn(ResponseEntity.ok().body("{}"));
        when(
            restTemplate.exchange(
                "/2",
                HttpMethod.GET, 
                null, 
                String.class
            )
        ).thenReturn(ResponseEntity.notFound().build());
        when(
            restTemplate.exchange(
                "/3",
                HttpMethod.GET, 
                null, 
                String.class
            )
        ).thenThrow(new RuntimeException("Exception"));
    }

    @Test
    public void testFindById() {
        assertTrue(httpCategoryRepository.findById(1).isPresent());
        assertFalse(httpCategoryRepository.findById(2).isPresent());
        assertFalse(httpCategoryRepository.findById(3).isPresent());
    }

    @Test
    public void testCacheableFindById() {
        assertDoesNotThrow(() -> httpCategoryRepository.cacheableFindById(1));
        assertThrows(ResponseStatusException.class, () -> httpCategoryRepository.cacheableFindById(2));
        assertThrows(RuntimeException.class, () -> httpCategoryRepository.cacheableFindById(3));
    }
}
