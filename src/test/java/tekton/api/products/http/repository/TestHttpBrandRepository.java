package tekton.api.products.http.repository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.RestTemplate;

import tekton.api.products.domain.model.dto.Brand;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

import com.fasterxml.jackson.databind.ObjectMapper;

@ExtendWith(SpringExtension.class)
public class TestHttpBrandRepository {
    @TestConfiguration
    static class TestHttpBrandRepositoryConf {
        @Bean
        public HttpBrandRepository HttpBrandRepository() {
            return new HttpBrandRepository();
        }

        @Bean 
        public RetryTemplate retryTemplate() {
            return new RetryTemplate();
        }
    }

    @Autowired HttpBrandRepository httpBrandRepository;
    @MockBean @Qualifier("brand-client") RestTemplate restTemplate;
    @MockBean ObjectMapper mapper;
    
    @BeforeEach
    public void mock() throws Throwable {
        when(mapper.readValue(any(String.class), eq(Brand.class))).thenReturn(new Brand());
        
        when(
            restTemplate.exchange(
                "/1",
                HttpMethod.GET, 
                null, 
                String.class
            )
        ).thenReturn(ResponseEntity.ok().body("{}"));
        when(
            restTemplate.exchange(
                "/2",
                HttpMethod.GET, 
                null, 
                String.class
            )
        ).thenReturn(ResponseEntity.notFound().build());
        when(
            restTemplate.exchange(
                "/3",
                HttpMethod.GET, 
                null, 
                String.class
            )
        ).thenThrow(new RuntimeException("Exception"));
    }

    @Test
    public void testFindById() {
        assertTrue(httpBrandRepository.findById(1).isPresent());
        assertFalse(httpBrandRepository.findById(2).isPresent());
        assertFalse(httpBrandRepository.findById(3).isPresent());
    }
}
